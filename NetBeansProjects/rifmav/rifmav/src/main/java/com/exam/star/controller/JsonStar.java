/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam.star.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author Star
 */
@RestController
public class JsonStar {

    @RequestMapping(value = "/test")
    public String test(@RequestParam String firstName,String lastName) {
        return firstName +"   "+lastName;
    }
    
    @RequestMapping(value = "/")
    public String test2() {
        return "Hello Api";
    }
}
