package com.exam.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Star
 */
@SpringBootApplication
public class Example {
   public static void main(String[] args) throws Exception {
        SpringApplication.run(Example.class, args);
   } 
}
