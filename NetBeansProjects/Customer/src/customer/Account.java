package customer;

import java.util.Scanner;

/**
 *
 * @author star
 */
public class Account {

    private double balance = 0;

    public static void main(String[] args) {
        int checkAccNum;
        int accountNumber = 0;

        double withdrawAmount;
        boolean exit = false;
        String accountName = null;
        String checkAccountName;
        Account customer = new Account();
        System.out.println("Please select any item from the list:");

        do {
            System.out.println("For create account press 1");
            System.out.println("For deposit account press 2");
            System.out.println("For withdraw account press 3");
            System.out.println("To exit press 0");
            Scanner scanner = new Scanner(System.in);
            String pressNum = scanner.next();

            if (pressNum.equals("1")) {

                System.out.println("Please Enter Your Account Name : ");
                accountName = scanner.next();
                accountNumber = (int) ((Math.random() * 100) + 100);
                System.out.println("A account number " + accountNumber + " has been created");

                System.out.println("Sir " + accountName + " an account " + accountNumber + " has been created for you");
                System.out.println("");

            } else if (pressNum.equals("2")) {

                System.out.println("Enter your account number to proceed : ");
                checkAccNum = scanner.nextInt();
                if (accountNumber == checkAccNum) {
                    double depositAmount;
                    System.out.println("Enter your Deposit ammount : ");
                    depositAmount = scanner.nextDouble();
                    depositAmount = customer.getBalance() + depositAmount;
                    customer.setBalance(depositAmount);
                    System.out.println("Deposite Successfull");
                    System.out.println("");
                    System.out.println("Your current balance is: " + customer.getBalance());
                    System.out.println("");
                } else {
                    System.out.println("Wrong account number");
                    System.out.println("");
                }

            } else if (pressNum.equals("3")) {

                System.out.println("Enter your account number to proceed : ");
                checkAccNum = scanner.nextInt();
                if (accountNumber == checkAccNum) {

                    System.out.println("Please Enter Your Withdraw Amount : ");
                    withdrawAmount = scanner.nextDouble();

                    if (customer.getBalance() >= withdrawAmount) {

                        customer.setBalance(customer.getBalance() - withdrawAmount);
                        System.out.println("Withdraw Successfull ");
                        System.out.println("");
                        System.out.println(" Your  current Balance is " + customer.getBalance());
                        System.out.println("");
                    } else {
                        System.out.println("Insufficient Balance, please deposit first");
                        System.out.println("");
                    }
                } else {
                    System.out.println("Wrong Account Name Or Number");
                }
            } else if (pressNum.equals("0")) {
                exit = true;
            }

        } while (!exit);

    }
    public double getNumber() {
        return balance;
    }
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    

}
